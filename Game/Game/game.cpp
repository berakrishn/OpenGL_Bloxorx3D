#include <iostream>
#include <cmath>
#include <math.h>
#include <fstream>
#include <vector>
#include <irrKlang.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace irrklang;
ISoundEngine* engine = createIrrKlangDevice();

struct VAO {
    GLuint VertexArrayID;
    GLuint VertexBuffer;
    GLuint ColorBuffer;

    GLenum PrimitiveMode;
    GLenum FillMode;
    int NumVertices;
};
typedef struct VAO VAO;

struct GLMatrices {
    glm::mat4 projection;
    glm::mat4 model;
    glm::mat4 view;
    GLuint MatrixID;
} Matrices;

int do_rot, floor_rel;;
GLuint programID;
double last_update_time, current_time;
glm::vec3 rect_pos, floor_pos;
float rectangle_rotation = 0;
int FLAG_UP = 0, FLAG_DOWN = 0, FLAG_RIGHT = 0, FLAG_LEFT = 0;
glm::vec3 eye, target, up;

int game_view = 1;  // 1: Tower, 2: Follow, 3: Ortho, 4: Free-look
float eye_x = 6, eye_y = 7, eye_z = 15;
float target_x = 0, target_y = 0, target_z = 0;
int game_moves = 0;
float game_time = 0.000, start_time = 0.000, end_time;

int level1_goal[2] = {9,5};
int bridge1[2] = {3,5};
int bridge1_cell1[] = {3,5}; // put in reverse: Z first then X

float zoom_scaling = 8.0;
float zoom_factor = 1;

int level1[12][12] = {{0,0,0,0,0,0,0,0,0,0,0,0},
                      {0,1,1,1,3,3,0,0,0,0,0,0},
                      {0,1,1,1,1,1,1,0,0,0,0,0},
                      {0,1,1,1,1,0,3,3,3,3,0,0},
                      {0,0,1,1,1,1,3,3,3,3,1,0},
                      {0,0,0,2,0,0,1,1,1,4,1,0},
                      {0,0,0,0,0,0,0,0,1,1,1,0},
                      {0,0,0,0,0,0,0,0,0,0,0,0},
                      {0,0,0,0,0,0,0,0,0,0,0,0},
                      {0,0,0,0,0,0,0,0,0,0,0,0},
                      {0,0,0,0,0,0,0,0,0,0,0,0},
                      {0,0,0,0,0,0,0,0,0,0,0,0}
                     };

class Tile
{
  public:
    Tile();
    ~Tile();
    VAO * VAO_Object;
    float x_cord;
    float y_cord;
    float z_cord;
    int colour;
    int type; // 1: regular, 2: bridge, 3: fragile, 4: goal
    float x_size;
    float y_size;
    float z_size;
};

vector< Tile > TILE_VECTOR;
Tile::Tile()
    : VAO_Object(new VAO()), x_cord(0), y_cord(0), z_cord(0), colour(1), type(1), x_size(1), y_size(2), z_size(1) {}
Tile::~Tile()
         {}

class Block
{
 public:
   Block();
   VAO * VAO_Object;
   float x_cord;
   float y_cord;
   float z_cord;
   int orientation; // 1: aligned along Y axis, 2: aligned along Z axis, 3: alogned along X axis
   int direction;
   int colour;
   float x_size;
   float y_size;
   float z_size;
};

vector< Block > BLOCK_VECTOR;
Block::Block()
   : VAO_Object(new VAO()), x_cord(0), y_cord(0), z_cord(0), orientation(1), direction(0), colour(1), x_size(1), y_size(1), z_size(1) {} // COMM: Update x,y,z Size

/* Function to load Shaders - Use it as it is */
GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path)
{

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open())
	{
	    std::string Line = "";
	    while(getline(VertexShaderStream, Line))
		VertexShaderCode += "\n" + Line;
	    VertexShaderStream.close();
	}

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
	std::string Line = "";
	while(getline(FragmentShaderStream, Line))
	    FragmentShaderCode += "\n" + Line;
	FragmentShaderStream.close();
    }

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    //    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> VertexShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
    //    fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

    // Compile Fragment Shader
    //    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
    //    fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);

    // Link the program
    //    fprintf(stdout, "Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> ProgramErrorMessage( max(InfoLogLength, int(1)) );
    glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
    //    fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

void quit(GLFWwindow *window)
{
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}

/* Generate VAO, VBOs and return VAO handle */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* color_buffer_data, GLenum fill_mode=GL_FILL)
{
    struct VAO* vao = new struct VAO;
    vao->PrimitiveMode = primitive_mode;
    vao->NumVertices = numVertices;
    vao->FillMode = fill_mode;

    // Create Vertex Array Object
    // Should be done after CreateWindow and before any other GL calls
    glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
    glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
    glGenBuffers (1, &(vao->ColorBuffer));  // VBO - colors

    glBindVertexArray (vao->VertexArrayID); // Bind the VAO
    glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices
    glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
    glVertexAttribPointer(
                          0,                  // attribute 0. Vertices
                          3,                  // size (x,y,z)
                          GL_FLOAT,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );

    glBindBuffer (GL_ARRAY_BUFFER, vao->ColorBuffer); // Bind the VBO colors
    glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), color_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
    glVertexAttribPointer(
                          1,                  // attribute 1. Color
                          3,                  // size (r,g,b)
                          GL_FLOAT,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );

    return vao;
}

/* Generate VAO, VBOs and return VAO handle - Common Color for all vertices */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat red, const GLfloat green, const GLfloat blue, GLenum fill_mode=GL_FILL)
{
    GLfloat* color_buffer_data = new GLfloat [3*numVertices];
    for (int i=0; i<numVertices; i++) {
        color_buffer_data [3*i] = red;
        color_buffer_data [3*i + 1] = green;
        color_buffer_data [3*i + 2] = blue;
    }

    return create3DObject(primitive_mode, numVertices, vertex_buffer_data, color_buffer_data, fill_mode);
}

/* Render the VBOs handled by VAO */
void draw3DObject (struct VAO* vao)
{
    // Change the Fill Mode for this object
    glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

    // Bind the VAO to use
    glBindVertexArray (vao->VertexArrayID);

    // Enable Vertex Attribute 0 - 3d Vertices
    glEnableVertexAttribArray(0);
    // Bind the VBO to use
    glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

    // Enable Vertex Attribute 1 - Color
    glEnableVertexAttribArray(1);
    // Bind the VBO to use
    glBindBuffer(GL_ARRAY_BUFFER, vao->ColorBuffer);

    // Draw the geometry !
    glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle
}

/**************************
 * Customizable functions *
 **************************/

float rectangle_rot_dir = 1;
bool rectangle_rot_status = true;

/* Executed when a regular key is pressed/released/held-down */
/* Prefered for Keyboard events */

void zoom_change(int a)
{
  if(zoom_scaling + zoom_factor*a <= 13 && zoom_scaling + zoom_factor*a >= 1.0)
    zoom_scaling += zoom_factor * a;
  Matrices.projection = glm::ortho(-zoom_scaling,zoom_scaling,-zoom_scaling,zoom_scaling,0.1f, 500.0f);
}

void keyboard (GLFWwindow* window, int key, int scancode, int action, int mods)
{
    // Function is called first on GLFW_PRESS.
  if (action == GLFW_RELEASE)
  {
    switch (key)
    {
  	case GLFW_KEY_C:
  	    rectangle_rot_status = !rectangle_rot_status;
  	    break;
    case GLFW_KEY_U:
        game_view = 1;
        break;
  	case GLFW_KEY_I:
        game_view = 2;
  	    break;
    case GLFW_KEY_O:
        game_view = 3;
        break;
    case GLFW_KEY_P:
        game_view = 4;
        break;
    case GLFW_KEY_A:
      eye_x -= 0.2;
    	break;
    case GLFW_KEY_D:
      eye_x += 0.2;
    	break;
    case GLFW_KEY_W:
      eye_z += 0.2;
    	break;
    case GLFW_KEY_S:
      eye_z -= 0.2;
    	break;
    case GLFW_KEY_R:
    	eye_y += 0.2;
    	break;
    case GLFW_KEY_F:
    	eye_y -= 0.2;
    	break;
  	default:
  	    break;
    }
  }
  else if (action == GLFW_PRESS)
  {
    switch (key)
    {
  	case GLFW_KEY_ESCAPE:
  	    quit(window);
  	    break;
    case GLFW_KEY_UP:
        FLAG_UP = 1;
        game_moves++;
        engine->play2D("../../media/bleep.mp3", false);
        break;
    case GLFW_KEY_DOWN:
        FLAG_DOWN = 1;
        game_moves++;
        engine->play2D("../../media/bleep.mp3", false);
        break;
    case GLFW_KEY_RIGHT:
        FLAG_RIGHT = 1;
        game_moves++;
        engine->play2D("../../media/bleep.mp3", false);
        break;
    case GLFW_KEY_LEFT:
        FLAG_LEFT = 1;
        game_moves++;
        engine->play2D("../../media/bleep.mp3", false);
        break;
  	default:
  	    break;
    }
  }
}

/* Executed for character input (like in text boxes) */
void keyboardChar (GLFWwindow* window, unsigned int key)
{
  switch (key)
  {
    case 'Q':
    case 'q':
    	quit(window);
    	break;
    default:
	    break;
    }
}

/* Executed when a mouse button is pressed/released */
void mouseButton (GLFWwindow* window, int button, int action, int mods)
{
    switch (button) {
    case GLFW_MOUSE_BUTTON_RIGHT:
	if (action == GLFW_RELEASE) {
	    rectangle_rot_dir *= -1;
	}
	break;
    default:
	break;
    }
}

void scroll(GLFWwindow* window, double xpos, double ypos)
{
  int x = int(xpos);
  int y = int(ypos);
  if(xpos == 0)zoom_change(-y);
}

void mouseHandler(GLFWwindow* window, double xpos, double ypos)
{
  double posx, posy;
  glfwGetCursorPos(window, &posx, &posy);
  if(game_view == 4)
  {
    posx -= 700/2;
    posy -= 700/2;
    posx = posx*1.0/350.0;
    posy = -posy*1.0/350.0;
    target_x += 0.10*posx;
    target_y += -0.10*posy;
    target_z += 0;
  }
}

/* Executed when window is resized to 'width' and 'height' */
/* Modify the bounds of the screen here in glm::ortho or Field of View in glm::Perspective */
void reshapeWindow (GLFWwindow* window, int width, int height)
{
    int fbwidth=width, fbheight=height;
    glfwGetFramebufferSize(window, &fbwidth, &fbheight);

    GLfloat fov = M_PI/2;

    // sets the viewport of openGL renderer
    glViewport (0, 0, (GLsizei) fbwidth, (GLsizei) fbheight);

    // Store the projection matrix in a variable for future use
    // Perspective projection for 3D views
    Matrices.projection = glm::perspective(fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1f, 500.0f);

    // Ortho projection for 2D views
    //Matrices.projection = glm::ortho(-4.0f, 4.0f, -4.0f, 4.0f, 0.1f, 500.0f);
    Matrices.projection = glm::ortho(-zoom_scaling, zoom_scaling, -zoom_scaling, zoom_scaling, 0.1f, 500.0f);
}

VAO *rectangle,*cam, *floor_vao;

// Creates the rectangle object used in this sample code
void createTile ()
{
    // GL3 accepts only Triangles. Quads are not supported
    static const GLfloat vertex_buffer_data [] = {
      -0.5, 0.1, 0.5,
    	-0.5, 0.0, 0.5,
    	0.5, 0.0, 0.5,
    	-0.5, 0.1, 0.5,
    	0.5, 0.0, 0.5,
    	0.5, 0.1, 0.5,
    	0.5, 0.1, 0.5,
    	0.5, 0.0, 0.5,
    	0.5, 0.0, -0.5,
    	0.5, 0.1, 0.5,
    	0.5, 0.0, -0.5,
    	0.5, 0.1, -0.5,
    	0.5, 0.1, -0.5,
    	0.5, 0.0, -0.5,
    	-0.5, 0.0, -0.5,
    	0.5, 0.1, -0.5,
    	-0.5, 0.0, -0.5,
    	-0.5, 0.1, -0.5,
    	-0.5, 0.1, -0.5,
    	-0.5, 0.0, -0.5,
    	-0.5, 0.0, 0.5,
    	-0.5, 0.1, -0.5,
    	-0.5, 0.0, 0.5,
    	-0.5, 0.1, 0.5,
    	-0.5, 0.1, -0.5,
    	-0.5, 0.1, 0.5,
    	0.5, 0.1, 0.5,
    	-0.5, 0.1, -0.5,
    	0.5, 0.1, 0.5,
    	0.5, 0.1, -0.5,
    	-0.5, 0.0, 0.5,
    	-0.5, 0.0, -0.5,
    	0.5, 0.0, -0.5,
    	-0.5, 0.0, 0.5,
    	0.5, 0.0, -0.5,
    	0.5, 0.0, 0.5
    };

    static const GLfloat color_buffer_data [] = {
      0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,
  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,

  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,
  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,

  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,
  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,

  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,
  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,

  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,
  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,

  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8,
  	0.4, 0.4, 0.4,
  	0.8, 0.8, 0.8,
  	0.8, 0.8, 0.8
    };

    float colourb1_r = 0.949, colourb1_g = 0.439, colourb1_b = 0.074;
    float colourb2_r = 0.556, colourb2_g = 0.239, colourb2_b = 0.015;
    static const GLfloat color_buffer_data_bridge [] = {
    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,
    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,

    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,
    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,

    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,
    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,

    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,
    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,

    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,
    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,

    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b,
    colourb1_r, colourb1_g, colourb1_b,
  	colourb2_r, colourb2_g, colourb2_b,
    colourb2_r, colourb2_g, colourb2_b
    };

    float colour1_r = 0.874, colour1_g = 0.074, colour1_b = 0.949;
    float colour2_r = 0.407, colour2_g = 0.011, colour2_b = 0.459;
    static const GLfloat color_buffer_data_fragile [] = {
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b
    };

    for(int i=0; i < TILE_VECTOR.size(); i++)
    {
      if(TILE_VECTOR[i].type == 1)
      {
        TILE_VECTOR[i].VAO_Object = create3DObject(GL_TRIANGLES, 36, vertex_buffer_data, color_buffer_data, GL_FILL);
      }
      else if(TILE_VECTOR[i].type == 2)
      {
        TILE_VECTOR[i].VAO_Object = create3DObject(GL_TRIANGLES, 36, vertex_buffer_data, color_buffer_data_bridge, GL_FILL);
      }
      else if(TILE_VECTOR[i].type == 3)
      {
        TILE_VECTOR[i].VAO_Object = create3DObject(GL_TRIANGLES, 36, vertex_buffer_data, color_buffer_data_fragile, GL_FILL);
      }
    }
}

void create_tile_map()
{
  float x_init_pos = 0, z_init_pos = 0;
  for(int i=1; i<12; i++)
  {
    for(int j=1; j<12; j++)
    {
      if(level1[i][j] == 1)
      {
        //if(i==level1_goal[0] && j==level1_goal[1])
        //else if(i==bridge1[0] && j==bridge1[1])
        Tile temp_tile;
        temp_tile.x_cord = z_init_pos + j * temp_tile.z_size;
        temp_tile.y_cord = 0;
        temp_tile.z_cord = x_init_pos + i * temp_tile.x_size;
        temp_tile.type = 1;
        TILE_VECTOR.push_back(temp_tile);
      }
      else if(level1[i][j] == 2)
      {
        Tile temp_tile;
        temp_tile.x_cord = z_init_pos + j * temp_tile.z_size;
        temp_tile.y_cord = 0;
        temp_tile.z_cord = x_init_pos + i * temp_tile.x_size;
        temp_tile.type = 2;
        TILE_VECTOR.push_back(temp_tile);
      }
      else if(level1[i][j] == 3)
      {
        Tile temp_tile;
        temp_tile.x_cord = j;
        temp_tile.y_cord = 0;
        temp_tile.z_cord = i;
        temp_tile.type = 3;
        TILE_VECTOR.push_back(temp_tile);
      }
      else if(level1[i][j] == 4)
      {
        Tile temp_tile;
        temp_tile.x_cord = z_init_pos + j * temp_tile.z_size;
        temp_tile.y_cord = 0;
        temp_tile.z_cord = x_init_pos + i * temp_tile.x_size;
        temp_tile.type = 4;
        TILE_VECTOR.push_back(temp_tile);
      }
    }
  }
}

void createBlock ()
{
    // GL3 accepts only Triangles. Quads are not supported
    static const GLfloat vertex_buffer_data [] = {
      -0.5, 1, 0.5,
    	-0.5, -1, 0.5,
    	0.5, -1, 0.5,
    	-0.5, 1, 0.5,
    	0.5, -1, 0.5,
    	0.5, 1, 0.5,
    	0.5, 1, 0.5,
    	0.5, -1, 0.5,
    	0.5, -1, -0.5,
    	0.5, 1, 0.5,
    	0.5, -1, -0.5,
    	0.5, 1, -0.5,
    	0.5, 1, -0.5,
    	0.5, -1, -0.5,
    	-0.5, -1, -0.5,
    	0.5, 1, -0.5,
    	-0.5, -1, -0.5,
    	-0.5, 1, -0.5,
    	-0.5, 1, -0.5,
    	-0.5, -1, -0.5,
    	-0.5, -1, 0.5,
    	-0.5, 1, -0.5,
    	-0.5, -1, 0.5,
    	-0.5, 1, 0.5,
    	-0.5, 1, -0.5,
    	-0.5, 1, 0.5,
    	0.5, 1, 0.5,
    	-0.5, 1, -0.5,
    	0.5, 1, 0.5,
    	0.5, 1, -0.5,
    	-0.5, -1, 0.5,
    	-0.5, -1, -0.5,
    	0.5, -1, -0.5,
    	-0.5, -1, 0.5,
    	0.5, -1, -0.5,
    	0.5, -1, 0.5
    };
    float colour1_r = 0.050, colour1_g = 0.737, colour1_b = 0.419;
    float colour2_r = 0.011, colour2_g = 0.298, colour2_b = 0.164;
    static const GLfloat color_buffer_data [] = {
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,

    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b,
    colour1_r, colour1_g, colour1_b,
  	colour2_r, colour2_g, colour2_b,
    colour2_r, colour2_g, colour2_b
    };

    // create3DObject creates and returns a handle to a VAO that can be used later
    BLOCK_VECTOR[0].VAO_Object = create3DObject(GL_TRIANGLES, 36, vertex_buffer_data, color_buffer_data, GL_FILL);
}

void createCam ()
{
    // GL3 accepts only Triangles. Quads are not supported
    static const GLfloat vertex_buffer_data [] = {
	-0.1, 0, 0,
	0.1, 0, 0,
	0, 0.1, 0,
    };

    static const GLfloat color_buffer_data [] = {
	1, 1, 1,
	1, 1, 1,
	1, 1, 1,
    };

    // create3DObject creates and returns a handle to a VAO that can be used later
    cam = create3DObject(GL_TRIANGLES, 1*3, vertex_buffer_data, color_buffer_data, GL_LINE);
}

float camera_rotation_angle = 90;

void update_view()
{
  if(game_view == 1)
  {
    eye_x = 6;
    eye_y = 7;
    eye_z = 15;
  }
  else if(game_view == 2)
  {
    target_x = BLOCK_VECTOR[0].x_cord;
    target_y = BLOCK_VECTOR[0].y_cord;
    target_z = BLOCK_VECTOR[0].z_cord;
    eye_x = target_x + 3;
    eye_y = target_y + 2;
    eye_z = target_z + 3;
  }
  else if(game_view == 3)
  {
    target_x = 4;
    target_y = 0;
    target_z = 4;
    eye_x = 4;
    eye_y = 15;
    eye_z = 7;
  }
  else if(game_view == 4)
  {
    //Do something
  }
}

void bridge()
{
  if(BLOCK_VECTOR[0].orientation == 1)
  {
    if(int(BLOCK_VECTOR[0].x_cord) == bridge1[0] && int(BLOCK_VECTOR[0].z_cord) == bridge1[1])
    {
      if(level1[bridge1_cell1[0]][bridge1_cell1[1]] == 0)
      {
        level1[bridge1_cell1[0]][bridge1_cell1[1]] = 1;
      }
      else
      {
        level1[bridge1_cell1[0]][bridge1_cell1[1]] = 0;
      }
      TILE_VECTOR.clear();
      create_tile_map();
    }
  }
  else if(BLOCK_VECTOR[0].orientation == 2)
  {
    if((int(BLOCK_VECTOR[0].x_cord) == bridge1[0] && int(BLOCK_VECTOR[0].z_cord) == bridge1[1]) || (int(BLOCK_VECTOR[0].x_cord) == bridge1[0] && int(BLOCK_VECTOR[0].z_cord + 1) == bridge1[1]))
    {
      if(level1[bridge1_cell1[0]][bridge1_cell1[1]] == 0)
      {
        level1[bridge1_cell1[0]][bridge1_cell1[1]] = 1;
      }
      else
      {
        level1[bridge1_cell1[0]][bridge1_cell1[1]] = 0;
      }
      TILE_VECTOR.clear();
      create_tile_map();
    }
  }
  else if(BLOCK_VECTOR[0].orientation == 3)
  {
    if((int(BLOCK_VECTOR[0].x_cord) == bridge1[0] && int(BLOCK_VECTOR[0].z_cord) == bridge1[1]) || (int(BLOCK_VECTOR[0].x_cord + 1) == bridge1[0] && int(BLOCK_VECTOR[0].z_cord) == bridge1[1]))
    {
      if(level1[bridge1_cell1[0]][bridge1_cell1[1]] == 0)
      {
        level1[bridge1_cell1[0]][bridge1_cell1[1]] = 1;
      }
      else
      {
        level1[bridge1_cell1[0]][bridge1_cell1[1]] = 0;
      }
      TILE_VECTOR.clear();
      create_tile_map();
    }
  }
}
/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw (GLFWwindow* window, float x, float y, float w, float h, int doM, int doV, int doP)
{
    int fbwidth, fbheight;
    glfwGetFramebufferSize(window, &fbwidth, &fbheight);
    glViewport((int)(x*fbwidth), (int)(y*fbheight), (int)(w*fbwidth), (int)(h*fbheight));

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram(programID);
    update_view();
    // Eye - Location of camera. Don't change unless you are sure!!
    eye = glm::vec3( eye_x, eye_y, eye_z);
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    target = glm::vec3(target_x, target_y, target_z);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    up = glm::vec3(0, 1, 0);

    // Compute Camera matrix (view)
    if(doV)
	Matrices.view = glm::lookAt(eye, target, up); // Fixed camera for 2D (ortho) in XY plane
    else
	Matrices.view = glm::mat4(1.0f);

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    glm::mat4 VP;
    if (doP)
	VP = Matrices.projection * Matrices.view;
    else
	VP = Matrices.view;

    glm::mat4 MVP;	// MVP = Projection * View * Model

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)

    for(int i=0; i<TILE_VECTOR.size(); i++)
    {
      Matrices.model = glm::mat4(1.0f);
      glm::mat4 translateTile = glm::translate (glm::vec3(TILE_VECTOR[i].x_cord ,TILE_VECTOR[i].y_cord, TILE_VECTOR[i].z_cord));
      Matrices.model *= (translateTile);
      MVP = VP * Matrices.model;
      glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
      draw3DObject(TILE_VECTOR[i].VAO_Object);
    }

    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translateBlock = glm::translate (glm::vec3(BLOCK_VECTOR[0].x_cord,BLOCK_VECTOR[0].y_cord, BLOCK_VECTOR[0].z_cord));
    Matrices.model *= (translateBlock);

    if(FLAG_UP == 1)
    {
      if(BLOCK_VECTOR[0].orientation == 1)
      {
        BLOCK_VECTOR[0].x_cord += 0;
        BLOCK_VECTOR[0].y_cord -= 0.5;
        BLOCK_VECTOR[0].z_cord -= 1.5;
        BLOCK_VECTOR[0].orientation = 2;
        BLOCK_VECTOR[0].direction = -1;
      }
      else if(BLOCK_VECTOR[0].orientation == 2)
      {
        BLOCK_VECTOR[0].x_cord += 0;
        BLOCK_VECTOR[0].y_cord += 0.5;
        BLOCK_VECTOR[0].z_cord -= 1.5;
        BLOCK_VECTOR[0].orientation = 1;
        BLOCK_VECTOR[0].direction = 0;
      }
      else if(BLOCK_VECTOR[0].orientation == 3)
      {
        BLOCK_VECTOR[0].x_cord += 0;
        BLOCK_VECTOR[0].y_cord += 0;
        BLOCK_VECTOR[0].z_cord -= 1;
        BLOCK_VECTOR[0].orientation = 3;
      }
      bridge();
      FLAG_UP = 0;
    }

    else if(FLAG_DOWN == 1)
    {
      if(BLOCK_VECTOR[0].orientation == 1)
      {
        BLOCK_VECTOR[0].x_cord += 0;
        BLOCK_VECTOR[0].y_cord -= 0.5;
        BLOCK_VECTOR[0].z_cord += 1.5;
        BLOCK_VECTOR[0].orientation = 2;
        BLOCK_VECTOR[0].direction = 1;
      }
      else if(BLOCK_VECTOR[0].orientation == 2)
      {
        BLOCK_VECTOR[0].x_cord += 0;
        BLOCK_VECTOR[0].y_cord += 0.5;
        BLOCK_VECTOR[0].z_cord += 1.5;
        BLOCK_VECTOR[0].orientation = 1;
        BLOCK_VECTOR[0].direction = 0;
      }
      else if(BLOCK_VECTOR[0].orientation == 3)
      {
        BLOCK_VECTOR[0].x_cord += 0;
        BLOCK_VECTOR[0].y_cord += 0;
        BLOCK_VECTOR[0].z_cord += 1;
        BLOCK_VECTOR[0].orientation = 3;
      }
      bridge();
      FLAG_DOWN = 0;
    }

    else if(FLAG_RIGHT == 1)
    {
      if(BLOCK_VECTOR[0].orientation == 1)
      {
        BLOCK_VECTOR[0].x_cord += 1.5;
        BLOCK_VECTOR[0].y_cord -= 0.5;
        BLOCK_VECTOR[0].z_cord -= 0;
        BLOCK_VECTOR[0].orientation = 3;
        BLOCK_VECTOR[0].direction = 1;
      }
      else if(BLOCK_VECTOR[0].orientation == 2)
      {
        BLOCK_VECTOR[0].x_cord += 1;
        BLOCK_VECTOR[0].y_cord += 0;
        BLOCK_VECTOR[0].z_cord += 0;
        BLOCK_VECTOR[0].orientation = 2;
      }
      else if(BLOCK_VECTOR[0].orientation == 3)
      {
        BLOCK_VECTOR[0].x_cord += 1.5;
        BLOCK_VECTOR[0].y_cord += 0.5;
        BLOCK_VECTOR[0].z_cord -= 0;
        BLOCK_VECTOR[0].orientation = 1;
        BLOCK_VECTOR[0].direction = 0;
      }
      bridge();
      FLAG_RIGHT = 0;
    }

    else if(FLAG_LEFT == 1)
    {
      if(BLOCK_VECTOR[0].orientation == 1)
      {
        BLOCK_VECTOR[0].x_cord -= 1.5;
        BLOCK_VECTOR[0].y_cord -= 0.5;
        BLOCK_VECTOR[0].z_cord -= 0;
        BLOCK_VECTOR[0].orientation = 3;
        BLOCK_VECTOR[0].direction = -1;
      }
      else if(BLOCK_VECTOR[0].orientation == 2)
      {
        BLOCK_VECTOR[0].x_cord -= 1;
        BLOCK_VECTOR[0].y_cord += 0;
        BLOCK_VECTOR[0].z_cord -= 0;
        BLOCK_VECTOR[0].orientation = 2;
      }
      else if(BLOCK_VECTOR[0].orientation == 3)
      {
        BLOCK_VECTOR[0].x_cord -= 1.5;
        BLOCK_VECTOR[0].y_cord += 0.5;
        BLOCK_VECTOR[0].z_cord -= 0;
        BLOCK_VECTOR[0].orientation = 1;
        BLOCK_VECTOR[0].direction = 0;
      }
      bridge();
      FLAG_LEFT = 0;
    }

    if(BLOCK_VECTOR[0].orientation == 2)
    {
      glm::mat4 rotateBlock = glm::rotate((float)(90.0f*M_PI/180.0f), glm::vec3(1,0,0));
      Matrices.model *= (rotateBlock);
    }
    else if(BLOCK_VECTOR[0].orientation == 3)
    {
      glm::mat4 rotateBlock = glm::rotate((float)(90.0f*M_PI/180.0f), glm::vec3(0,0,1));
      Matrices.model *= (rotateBlock);
    }
    MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(BLOCK_VECTOR[0].VAO_Object);


}

/* Initialise glfw window, I/O callbacks and the renderer to use */
/* Nothing to Edit here */
GLFWwindow* initGLFW (int width, int height){
    GLFWwindow* window; // window desciptor/handle

    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "Graphics Assignment-2: Krishn Bera", NULL, NULL);

    if (!window) {
	exit(EXIT_FAILURE);
        glfwTerminate();
    }

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval( 1 );
    glfwSetFramebufferSizeCallback(window, reshapeWindow);
    glfwSetWindowSizeCallback(window, reshapeWindow);
    glfwSetWindowCloseCallback(window, quit);
    glfwSetKeyCallback(window, keyboard);      // general keyboard input
    glfwSetCharCallback(window, keyboardChar);  // simpler specific character handling
    glfwSetScrollCallback(window,scroll);
    glfwSetCursorPosCallback(window, mouseHandler);
    glfwSetMouseButtonCallback(window, mouseButton);  // mouse button clicks

    return window;
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL (GLFWwindow* window, int width, int height)
{
    /* Objects should be created before any other gl function and shaders */
    // Create the models
    createTile ();
    createBlock();
    createCam();

    // Create and compile our GLSL program from the shaders
    programID = LoadShaders( "Sample_GL.vert", "Sample_GL.frag" );
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (0.3f, 0.3f, 0.3f, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);
}

void game_end()
{
  game_time = end_time - start_time;
  cout << "\n\n\n**************************************" << endl;
  cout << "Number of Moves: " << game_moves << "\nTime Taken(seconds): " << game_time << endl;
  cout << "**************************************" << endl;
}

void checkFall()
{
  //cout << "$$$$ " << BLOCK_VECTOR[0].x_cord << " " << BLOCK_VECTOR[0].z_cord << endl;
  //cout << int(BLOCK_VECTOR[0].x_cord) << " * " << int(BLOCK_VECTOR[0].z_cord)  << " * " << level1[int(BLOCK_VECTOR[0].x_cord)][int(BLOCK_VECTOR[0].z_cord)] << endl;
  if(BLOCK_VECTOR[0].orientation == 1)
  {
    if(level1[int(BLOCK_VECTOR[0].z_cord)][int(BLOCK_VECTOR[0].x_cord)] == 3)
    {
      cout << "GAME OVER." << endl;
      game_end();
      exit(EXIT_SUCCESS);
    }
    if(int(BLOCK_VECTOR[0].x_cord) == level1_goal[0] && int(BLOCK_VECTOR[0].z_cord) == level1_goal[1])
    {
      cout << "LEVEL COMPLETE." << endl;
      game_end();
      exit(EXIT_SUCCESS);
    }
    if(level1[int(BLOCK_VECTOR[0].z_cord)][int(BLOCK_VECTOR[0].x_cord)] == 0)
    {
      cout << "GAME OVER." << endl;
      game_end();
      exit(EXIT_SUCCESS);
    }
  }
  else if(BLOCK_VECTOR[0].orientation == 2)
  {
    if(level1[int(BLOCK_VECTOR[0].z_cord)][int(BLOCK_VECTOR[0].x_cord)] == 0 || level1[int(BLOCK_VECTOR[0].z_cord + 0.5)][int(BLOCK_VECTOR[0].x_cord)] == 0)
    {
      cout << "GAME OVER." << endl;
      game_end();
      exit(EXIT_SUCCESS);
    }
  }
  else if(BLOCK_VECTOR[0].orientation == 3)
  {
    if(level1[int(BLOCK_VECTOR[0].z_cord)][int(BLOCK_VECTOR[0].x_cord)] == 0 || level1[int(BLOCK_VECTOR[0].z_cord )][int(BLOCK_VECTOR[0].x_cord+1)] == 0)
    {
      cout << "GAME OVER." << endl;
      game_end();
      exit(EXIT_SUCCESS);
    }
  }
}

int main (int argc, char** argv)
{
    ISoundEngine* engine1 = createIrrKlangDevice();
    if (!engine)
    {
    printf("Could not startup engine\n");
    return 0;
    }
    engine1->play2D("../../media/ophelia.mp3", true);

    int width = 700;
    int height = 700;
    rect_pos = glm::vec3(0, 0, 0);
    floor_pos = glm::vec3(0, -2, 0);
    do_rot = 0;
    floor_rel = 1;

    Block temp_block;
    if(temp_block.orientation == 1)
    {
    temp_block.x_cord = 2;
    temp_block.y_cord = 1;
    temp_block.z_cord = 2;
    }
    BLOCK_VECTOR.push_back(temp_block);
    target_x = BLOCK_VECTOR[0].x_cord;
    target_y = BLOCK_VECTOR[0].y_cord;
    target_z = BLOCK_VECTOR[0].z_cord;
    GLFWwindow* window = initGLFW(width, height);
    initGL (window, width, height);
    last_update_time = glfwGetTime();
    start_time = glfwGetTime();
    create_tile_map();
    while (!glfwWindowShouldClose(window))
    {
    	// clear the color and depth in the frame buffer
    	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // OpenGL Draw commands
    	current_time = glfwGetTime();
      //cout << current_time << " " << last_update_time << endl;

    	draw(window, 0, 0, 1, 1, 1, 1, 1);
      createTile();
      createBlock();
      checkFall();
      // Swap Frame Buffer in double buffering
      glfwSwapBuffers(window);

      // Poll for Keyboard and mouse events
      glfwPollEvents();
      end_time = glfwGetTime();
    }
    engine->drop();
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
